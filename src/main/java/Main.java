import com.javagda18.model.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.time.LocalDateTime;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        Indeks indeksOcen = new Indeks();
//        indeksOcen.setStudentIndeks("123123");
//        // stworzyłem indeks
//
//        Student student1 = new Student("a", "b", "123123", 21, indeksOcen);
//        indeksOcen.setStudent(student1);
//        // stworzyłem studenta i powiązałem ze sobą te instancje
//
//        System.out.println(student1.toString());
//
        Student student = new Student("Pawel", "Gawel", "123123", 20);
        Teacher teacher = new Teacher("ticzer", "lastnejm", LocalDateTime.of(2000, 05, 30, 15, 00), 20, TeacherType.GIMNAZJUM);

        // ## Tworzenie relacji między obiektami
        // 1. zapis obu encji.
        Dao.saveEntity(teacher);
        Dao.saveEntity(student);

        // 2. powiązanie relacji
        teacher.setStudent(student);
        student.setTeacher(teacher);

        // 3. zapis encji
        Dao.saveEntity(teacher);
        Dao.saveEntity(student);

        List<Student> studentList = Dao.getAll(Student.class);
        System.out.println(studentList);

        List<Teacher> teacherList = Dao.getAll(Teacher.class);
        System.out.println(teacherList);

    }
}
