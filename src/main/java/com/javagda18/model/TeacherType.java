package com.javagda18.model;

public enum  TeacherType {
    UCZELNIA_WYZSZA, SZKOLA_PODSTAWOWA, GIMNAZJUM, LICEUM;
}
