package com.javagda18.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    // TABLE - identyfikatory będą generowane w obrębie tabeli
    // oba działają w oparciu o hibernate sequence
    // IDENTITY - najpierw sprawdzamy dostępne id, potem tworzymy obiekt i przydzielamy id i zapisujemy z tym id
    // SEQUENCE - zapisujemy z null id i pytamy o przydzielone id
    // null - wygeneruje mi sie indeks
    private Long id; // AUTO_INCwREMENT

    public Student(String imie, String nazwisko, String indeks, int wiek) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.indeks = indeks;
        this.wiek = wiek;
    }

    private String imie;
    private String nazwisko;

    private String indeks;  // NIE INDEX!
    private int wiek;

    // jeden student posiada jednego teachera
    @OneToOne
    @ToString.Exclude
    private Teacher teacher;

    // Wielu studentów posiada jednego teachera
    // @ManyToOne
}
