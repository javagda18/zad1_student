package com.javagda18.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Teacher extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Teacher(String name, String surname, LocalDateTime hireDate, int yearsExperience, TeacherType teacherType) {
        this.name = name;
        this.surname = surname;
        this.hireDate = hireDate;
        this.yearsExperience = yearsExperience;
        this.teacherType = teacherType;
    }

    private String name;
    private String surname;
    private LocalDateTime hireDate;
    private int yearsExperience;

    @Enumerated(EnumType.STRING)
    private TeacherType teacherType;

    // jeden teacher posiada jednego studenta
    @OneToOne
    @ToString.Exclude
    private Student student;

}
